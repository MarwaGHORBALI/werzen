import { authHeader } from '../../../_helpers';
import { environement } from '../../../environement/environement'

export const openService = {
    /**
     *** WS GET ***
     **/
    getUsersById,
    getHousesOffers,
    getHousesPopular,
    getListOffers,
    getOffersById,
    getListHouses
   
};


//G.M : ws get users by id 
function getUsersById(id) {
    const requestOptions = {
        method: 'GET',
    };
    return fetch(`${environement.OPEN}/users/${id}`, requestOptions).then(handleResponse);
}
 

//G.M : ws get houses offers 
function getHousesOffers() {
    const requestOptions = {
        method: 'GET',
    };
    return fetch(`${environement.OPEN}/houses/offers`, requestOptions).then(handleResponse);
}


//G.M : ws get Houses Popular 
function getHousesPopular() {
    const requestOptions = {
        method: 'GET',
    };
    return fetch(`${environement.OPEN}/houses/popular`, requestOptions).then(handleResponse);
}


//G.M : ws get offers List 
function getListOffers() {
    const requestOptions = {
        method: 'GET',
    };
    return fetch(`${environement.OPEN}/offers/list`, requestOptions).then(handleResponse);
}

//G.M : ws get offers by id 
function getOffersById(id) {
    const requestOptions = {
        method: 'GET',
    };
    return fetch(`${environement.OPEN}/offers/${id}`, requestOptions).then(handleResponse);
}


//G.M : ws get houses List 
function getListHouses() {
    const requestOptions = {
        method: 'GET',
    };
    return fetch(`${environement.OPEN}/houses/list`, requestOptions).then(handleResponse);
}






function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
}
/*

function getAll() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${environement.API}/users`, requestOptions).then(handleResponse);
}*/

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                // location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}