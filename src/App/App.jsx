import React from 'react';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { history } from '../_helpers';
import { alertActions } from '../_actions';
import { PrivateRoute } from '../_components';
import { DashboardPage } from '../_components/dashboardPage';
import { DetailsHomePage } from '../_components/detailsHomePage'



import { LoginPage } from '../LoginPage';
import Footer from './Footer';

//className="jumbotron"

class App extends React.Component {
    constructor(props) {
        super(props);

        history.listen((location, action) => {
            // clear alert on location change
            this.props.clearAlerts();
        });
    }

    render() {
        const { alert } = this.props;
        return (
            <div >
             
               
                        {alert.message &&
                            <div className={`alert ${alert.type}`}>{alert.message}</div>
                        }
                        <Router history={history}>
                            <Switch>
                                <PrivateRoute exact path="/" component={DashboardPage} />
                                <Route path="/dashboard" component={DashboardPage} />
                                <Route  path= "/details/:id" component= {DetailsHomePage} />
                           
                                <Redirect from="*" to="/" />
                            </Switch>
                        </Router>
                    
                        <br />
                        <Footer />
            </div>
        );
    }
}

function mapState(state) {
    const { alert } = state;
    return { alert };
}

const actionCreators = {
    clearAlerts: alertActions.clear
};

const connectedApp = connect(mapState, actionCreators)(App);
export { connectedApp as App };