import React, { Component } from 'react';
import Card from "react-bootstrap/Card";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import { FiAlignLeft, FiFilter } from "react-icons/fi";
import { history } from '../../_helpers';
import { Redirect } from 'react-router';


import { Router, Route, Link } from 'react-router-dom';


class ListMaisonDisp extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleClick = this.handleClick.bind(this);
        
    }

    handleChange(e) {
        this.setState({ value: e.target.value });
    }



    handleClick(id ,houseData) {
        history.push(`/details/${id}`);
 
        
        console.log('Click happened');
       
    }


    render() {
        const { listHouses } = this.props
        return (
     
            <Container>
           
                <Row>
                    <Col className="Maison-dhte-disponible">
                        Maison d’hôte disponible
                </Col>
                    <Col>
                        <Row >
                            <FiFilter className="Icon-feather-filter" />
                            <FiAlignLeft className="Icon-material-sort" />
                        </Row>
                    </Col>
                </Row>


                {listHouses.houseList &&
                    <Row>
                        {listHouses.houseList.map((house, index) =>
                            <Col key={house.id}>
                                <Card style={{ width: '18rem' }} className="card-houses-disp" >
                                    <Card.Img variant="top" src={house.cover.web_path} onClick={()=>this.handleClick(house.id,house)} />
                                    <Card.Body>
                                        <Card.Title>{house.name}</Card.Title>
                                        <Card.Text>
                                            {house.description}
                                        </Card.Text>
                                        <Card.Link href="#">Card Link</Card.Link>
                                    </Card.Body>
                                </Card>
                            </Col>
                        )}


                        
                    </Row>
                }
                
                <br />    <br />

                <div class="col-lg-3 col-md-3 col-sm-12 p-0">
                    <button type="button" class="btn  wrn-btn btn-aff">Afficher Tous</button>
                </div>
          

          
            </Container>
           

        );
    }
}
export default ListMaisonDisp;