import styled from 'styled-components';
import { palette } from 'styled-theme';
//import bgCarte from '../../image/images/bgcarte.jpg';
import WithDirection from '../../config/withDirection';

const SignInStyleWrapper = styled.div`
  width: 100%;
  min-height: 100vh;
  //height: 100vh;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  position: relative;
  background-size: cover;

  &:before {
    content: '';
    width: 100%;
    height: 100%;
    display: flex;
    position: absolute;
    z-index: 1;
    top: 0;
    left: ${props => (props['data-rtl'] === 'rtl' ? 'inherit' : '0')};
    right: ${props => (props['data-rtl'] === 'rtl' ? '0' : 'inherit')};
  }

  .carousel-inner {
    height: 800px;
  }
  .bg-carte{
   
    width: 1364px;
  height: 300px;
  object-fit: contain;
  //opacity: 0.2;
  border-radius: 5px;
  margin :auto;
  }
 

  /*search box css start here*/
.search-sec{
  z-index:10;
    padding: 3rem;
    width: 1364px;
  height: 155px;
  border-radius: 5px;
  background-color: #ffffff;
  margin:auto;
}

.navBar-sec{
  top: 90px;
  z-index: 10; 
  background: transparent !important;

}


.nav-link{
  height: 28px;
  font-family: Poppins;
  font-size: 20px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.5;
  letter-spacing: normal;
  text-align: left;
  color: #ffffff !important;

}

.search-slt{
    display: block;
    width: 100%;
    font-size: 0.875rem;
    line-height: 1.5;
    color: #55595c;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    height: calc(3rem + 2px) !important;
    border-radius:0;
}
.SUNSET-VILLA{
  width: 662px;
 // height: 140px;
  font-family: Poppins;
  font-size: 100px;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.5;
  letter-spacing: normal;
  text-align: left;
  color: #ffffff;

}
.carousel-control-next-icon{
  width: 20px;
  height: 40px;
}
.carousel-control-prev-icon {
  background-image: unset;
}
.wrn-btn{
    width: 100%;
    font-size: 16px;
    font-weight: 400;
    text-transform: capitalize;
    height: calc(3rem + 2px) !important;
    border-radius:0;
}
.Maison-dhte-disponible.col{
  width: 447px;
  height: 49px;
  font-family: Poppins;
  font-size: 35px;
  font-weight: 600;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.51;
  letter-spacing: normal;
  text-align: left;
  color: #555555;
}
.card{
  border :none !important;
  z-index:10;
  &.card-houses{
    width: 260px !important;
    height: 325px;
    object-fit: contain;
    border-radius: 5px;
    .card-body{
      .card-text{
        width: 260px;
        height: 67px;
        font-family: Poppins;
        font-size: 17px;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.18;
        letter-spacing: normal;
        text-align: left;
        color: #919191;

      }
      .card-title{
        width: 260px;
  height: 35px;
  font-family: Poppins;
  font-size: 25px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.52;
  letter-spacing: normal;
  text-align: left;
  color: #555555;


      }
    }

  }

  &.card-houses-disp{
    width: 443px important;
    height: 275px;
    object-fit: contain;
    border-radius: 5px;
    .card-body{
      .card-text{
        width: 443px;
  height: 48px;
  font-family: Poppins;
  font-size: 17px;
  font-weight: normal;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.18;
  letter-spacing: normal;
  text-align: left;
  color: #919191;
      }
      .card-title{
        width: 198px;
  height: 35px;
  font-family: Poppins;
  font-size: 25px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.52;
  letter-spacing: normal;
  text-align: left;
  color: #555555;
      }
    }

  }
  &.card-offers{
  width: 674px;
  //height: 275px;
  object-fit: contain;
  border-radius: 5px;

  .card-body{
    .card-text{
      width: 674px;
    height: 45px;
    font-family: Poppins;
    font-size: 17px;
    font-weight: normal;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.18;
    letter-spacing: normal;
    text-align: left;
    color: #919191;
    }
    .card-title{
      width: 191px;
height: 35px;
font-family: Poppins;
font-size: 25px;
font-weight: 500;
font-stretch: normal;
font-style: normal;
line-height: 1.52;
letter-spacing: normal;
text-align: left;
color: #555555;
    }
  }
  }
}
@media (min-width: 992px){
    .search-sec{
        position: relative;
        top: -77px;
        background: rgba(26, 70, 104, 0.51);
        z-index:10;
        width: 1200px;
    height: 140px;
  border-radius: 5px;
  background-color: #ffffff;
  margin:auto;
    }
    .card{
      border :none !important;
      &.card-offers{
      width: 474px !important;
      //height: 275px;
      object-fit: contain;
      border-radius: 5px;
     
      .card-body{
        .card-text{
          width: 474px;
        height: 45px;
        font-family: Poppins;
        font-size: 17px;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.18;
        letter-spacing: normal;
        text-align: left;
        color: #919191;
        }
    
      }
    }
    }
    .navBar-sec{
      position: relative;
      background: rgba(26, 70, 104, 0.51);
      top: 90px;
      z-index: 10; 
    
    }
    .Maison-dhte-disponible.col{
      font-size:25px;

    }
    .carousel-inner {
      height: 600px;
    }
    .bg-carte{
     
      width: 1200px;
    height: 200px;
   
    
    }
    .SUNSET-VILLA{
      font-size:70px;
    }
    .Amazing-view{
      width: 501px;
  height: 28px;
  font-family: Poppins;
  font-size: 20px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.5;
  letter-spacing: normal;
  text-align: left;
  color: #ffffff;
    }
    .btn{
      z-index:10;
      width: 260px;
  height: 70px;
  border-radius: 5px;
  background-color: #27ac90;
  font-family:Poppins;
  font-size: 25px;
  font-weight: 500;
  font-stretch: normal;
  font-style: normal;
  line-height: 1.52;
  letter-spacing: normal;
  color: #ffffff;
  &.btn-aff{
    width: 260px;
    height: 70px;
    border-radius: 5px;
    border: solid 2px #27ac90;
    background-color: #fff;
    margin:auto !important ;
    font-family: Poppins;
    font-size: 20px;
    font-weight: 500;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.5;
    letter-spacing: normal;
    text-align: center;
    color: #27ac90;

  }
  
    }
    .Icon-feather-filter,
    .Icon-material-sort{
      width: 30px;
      height: 28px;

      color: #d1d1d1;
    }
}

@media (max-width: 992px){
    .search-sec{
        background: #1A4668;
        z-index:10;
        width: 1200px;
       height: 140px;
  border-radius: 5px;
  background-color: #ffffff;
  margin:auto;
    }
    .card{
      border :none !important;
       z-index:10;
      &.card-offers{
      width: 474px !important;
      //height: 275px;
      object-fit: contain;
      border-radius: 5px;
   
      .card-body{
        .card-text{
          width: 474px;
        height: 45px;
        font-family: Poppins;
        font-size: 17px;
        font-weight: normal;
        font-stretch: normal;
        font-style: normal;
        line-height: 1.18;
        letter-spacing: normal;
        text-align: left;
        color: #919191;
        }
        .card-title{
          width: 191px;
    height: 35px;
    font-family: Poppins;
    font-size: 25px;
    font-weight: 500;
    font-stretch: normal;
    font-style: normal;
    line-height: 1.52;
    letter-spacing: normal;
    text-align: left;
    color: #555555;
        }
    
      }
    }
    }
    .Maison-dhte-disponible.col{
      font-size:25px;

    }
    .navBar-sec{
      background: #1A4668;
      background: #1A4668;
      top: 90px;
      z-index: 10; 
    
    }
    .carousel-inner {
      height: 600px;

    }
    .bg-carte{
     
   width: 1200px;
     height: 200px;
    
     
     }
     .SUNSET-VILLA{
      font-size:70px;
    }
}
  
.navbar-wrapper {
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  z-index: 10;
}

.overlay { 
  color:#fff;
  position:absolute;
  z-index:12;
  top:50%;
  left:0;
  width:100%;
  text-align:center;
}

  .isoLoginContentWrapper {
    width: 500px;
    height: 100%;
    overflow-y: auto;
    z-index: 10;
    position: relative;
  }

  .carousel-caption{
    bottom: 400px;
    left: 200px;
    z-index: 10;
    padding-top: 0px;
    padding-bottom: 0px;

  }
  

  .isoLoginContent {
    width : 450px;
    height: 430px;
    display: flex;
    flex-direction: column;
    padding: 30px 50px;
    position: relative;
    background-color: #ffffff;
    z-index: 1;
    margin:auto;
    .logo-aio{
      text-align : center;
      img{
        width : 130px !important ;
      }
    }
    .btn-login{
      text-align:end;
      .root-50{
        background: #14A1F0 !important;
        border: none !important;
        border-radius:0px !important;
        :hover{
          background: #3A7BD5 !important;

        }
      }
    }
    .form-control{
      border-radius: inherit !important;
    }

    @media only screen and (max-width: 767px) {
      width: 100%;
      padding: 70px 20px;
    }
    .Maison-dhte-disponible.col{
      font-size:25px;

    }
  

    .isoLogoWrapper {
      width: 100%;
      display: flex;
      margin-bottom: 50px;
      justify-content: center;
      flex-shrink: 0;

      a {
        font-size: 24px;
        font-weight: 300;
        line-height: 1;
        text-transform: uppercase;
        color: ${palette('secondary', 2)};
      }
    }

    .isoSignInForm {
      width: 100%;
      display: flex;
      flex-shrink: 0;
      flex-direction: column;

      .isoInputWrapper {
        margin-bottom: 15px;

        &:last-of-type {
          margin-bottom: 0;
        }

        input {
          &::-webkit-input-placeholder {
            color: ${palette('grayscale', 0)};
          }

          &:-moz-placeholder {
            color: ${palette('grayscale', 0)};
          }

          &::-moz-placeholder {
            color: ${palette('grayscale', 0)};
          }
          &:-ms-input-placeholder {
            color: ${palette('grayscale', 0)};
          }
        }
      }

      .isoHelperText {
        font-size: 12px;
        font-weight: 400;
        line-height: 1.2;
        color: ${palette('grayscale', 1)};
        padding-left: ${props =>
    props['data-rtl'] === 'rtl' ? 'inherit' : '13px'};
        padding-right: ${props =>
    props['data-rtl'] === 'rtl' ? '13px' : 'inherit'};
        margin: 15px 0;
        position: relative;
        display: flex;
        align-items: center;

        &:before {
          content: '*';
          color: ${palette('error', 0)};
          padding-right: 3px;
          font-size: 14px;
          line-height: 1;
          position: absolute;
          top: 2px;
          left: ${props => (props['data-rtl'] === 'rtl' ? 'inherit' : '0')};
          right: ${props => (props['data-rtl'] === 'rtl' ? '0' : 'inherit')};
        }
      }

      .isoHelperWrapper {
        margin-top: 35px;
        flex-direction: column;
      }

      .isoOtherLogin {
        padding-top: 40px;
        margin-top: 35px;
        border-top: 1px dashed ${palette('grayscale', 2)};

        > a {
          display: flex;
          margin-bottom: 10px;

          &:last-child {
            margin-bottom: 0;
          }
        }

        button {
          width: 100%;
          height: 42px;
          border: 0;
          font-weight: 500;

          &.btnFacebook {
            background-color: #3b5998;

            &:hover {
              background-color: darken(#3b5998, 5%);
            }
          }

          &.btnGooglePlus {
            background-color: #dd4b39;
            margin-top: 15px;

            &:hover {
              background-color: darken(#dd4b39, 5%);
            }
          }

          &.btnAuthZero {
            background-color: #e14615;
            margin-top: 15px;

            &:hover {
              background-color: darken(#e14615, 5%);
            }
          }

          &.btnFirebase {
            background-color: ${palette('color', 5)};
            margin-top: 15px;

            &:hover {
              background-color: ${palette('color', 6)};
            }
          }
        }
      }

      .isoForgotPass {
        font-size: 12px;
        color: ${palette('text', 3)};
        margin-bottom: 10px;
        text-decoration: none;

        &:hover {
          color: ${palette('primary', 0)};
        }
      }

      button {
        font-weight: 500;
      }
    }
  }
`;

export default WithDirection(SignInStyleWrapper);
