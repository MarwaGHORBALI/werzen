import React from 'react';
import * as ReactDOM from 'react-dom';
import { Router, Route, Link } from 'react-router-dom';


import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';
//redux
import { connect } from 'react-redux';
//actions
import { openActions } from '../../_actions';
import { alertActions } from '../../_actions';
import { history } from '../../_helpers';





//components
import Header from './Header';
import ListMaison from './ListMaison';
import CarteBar from './CarteBar';
import ListOffers from './ListOffers';
import ListMaisonDisp from './ListMaisonDisp';


//styles
import SignInStyleWrapper from './dashboard.style.js';




class DashboardPage extends React.Component {

    constructor(props) {
        super(props);

    }
    componentDidMount(){
        //this.props.getHouses();
        //this.props.getListOffers();

    }

    componentWillMount() {
       this.props.getHouses();
        this.props.getListOffers();
        
    }



    render() {
        const { housesList, open } = this.props;
       
        return (
         
            <Formik
                initialValues={{
                    email: '',
                    password: '',
                }}

                validationSchema={Yup.object().shape({
                    email: Yup.string()
                        .email('Email is invalid')
                        .required('Email is required'),
                    password: Yup.string()
                        .required('Password is required'),

                })}

                onSubmit={fields => {
                    alert('SUCCESS!! :-)\n\n' + JSON.stringify(fields, null, 4))
                }}

                render={({ errors, status, touched }) => (
                    <SignInStyleWrapper className="isoSignInPage">
                        <div className="wrapper">
                            <Header />
                            <div className="workspace">
                                <ListMaisonDisp listHouses={housesList} /> <br />
                                <CarteBar /> <br />

                                <ListMaison listHouses={housesList} /> <br />
                                <ListOffers listOffer={open} />  <br />  
                                                       
                            </div>
                        </div>
                    </SignInStyleWrapper>
                )}
            />
            
        )
        
     
    }
}





function mapState(state) {
    const { housesList, open } = state;
    return { housesList, open };
}

const actionCreators = {
    //  clearAlerts: alertActions.clear,
    getHouses: openActions.getListHouses,
    getListOffers: openActions.getListOffers,

};

const connectedDashboard = connect(mapState, actionCreators)(DashboardPage);
export { connectedDashboard as DashboardPage };



