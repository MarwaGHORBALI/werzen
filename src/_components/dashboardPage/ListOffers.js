import React, { Component } from 'react';
import Card from "react-bootstrap/Card";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import { FiAlignLeft, FiFilter } from "react-icons/fi";
import { openService } from '../../_shared/services/open/open.service';

//redux
import { connect } from 'react-redux';
//actions
import { openActions } from '../../_actions';

class ListOffers extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);

    }

  
    handleChange(e) {
        this.setState({ value: e.target.value });
    }




    render() {
        const {listOffer } = this.props;
        return (
            <Container>
                <Row>
                    <Col className="Maison-dhte-disponible">
                        Werzen Lifestyle
                </Col>
                </Row>
                {listOffer.offers &&
                    <Row> 
                        {listOffer.offers.map((offer, index) =>
                            <Col key={offer.id}>
                                <Card style={{ width: '18rem' }} className="card-offers">
                                    <Card.Img variant="top" src={offer.images[0].web_path} />
                                    <Card.Body>
                                        <Card.Title>{offer.title}</Card.Title>
                                        <Card.Text>
                                            {offer.description}
                                        </Card.Text>

                                    </Card.Body>
                                </Card>
                            </Col>
                        )}
                    </Row>
                }
                <div class="col-lg-3 col-md-3 col-sm-12 p-0">
                    <button type="button" class="btn  wrn-btn btn-aff">Afficher Tous</button>
                </div>
            </Container>

        );
    }
}

export default ListOffers;

