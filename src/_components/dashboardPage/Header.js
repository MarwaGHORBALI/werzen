import React, { Component } from 'react';
//import * as React from 'react';
import * as ReactDOM from 'react-dom';

import Logowerzen from '../../image/images/logo/Logowerzen.png';
import sliderone from '../../image/images/sliderimgs/sliderone.png';


import Carousel from 'react-bootstrap/Carousel';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
//import NavDropdown from 'react-bootstrap/NavDropdown';
import Image from 'react-bootstrap/Image';
//import { NavLink } from "react-router-dom";
import { FiMapPin, FiUsers } from "react-icons/fi";


import DatePickers from '../../App/DatePickers';

class Header extends Component {



  render() {

  

    return (
      <header>

        <section class="navBar-sec">
          <div class="container">
            <form >

              <Navbar collapseOnSelect expand="lg" variant="dark">
                <Navbar.Brand href="#home">
                  <img
                    src={Logowerzen}
                    width="110"
                    height="70"
                    className="d-inline-block align-top"
                    alt="React Bootstrap logo"
                  />
                </Navbar.Brand>

                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                  <Nav className="mr-auto">
                  </Nav>
                  <Nav>
                    <Nav.Link href="#deets">Nos offeres</Nav.Link>
                    <Nav.Link eventKey={2} href="#memes">
                      Carte cadeau
                    </Nav.Link>
                    <Nav.Link eventKey={2} href="#memes">
                      Destination
                    </Nav.Link>
                    <Nav.Link href="#deets"> <Image src={require('../../image/images/users.png')} roundedCircle width="55" /></Nav.Link>

                  </Nav>
                </Navbar.Collapse>
              </Navbar>

            </form>
          </div>
        </section>

        <section>
          <Carousel className="hello-lightbulb-YC8qqp50BdA-unsplash" >
            <Carousel.Item>

              <img
                className="d-block w-100"
                src={sliderone}
                height="800"
                alt="First slide"
              />
              <Carousel.Caption>

                <h3 className="SUNSET-VILLA" >SUNSET VILLA</h3>
                <p className="Amazing-view">Amazing view - Cave house - SPA - Breakfast incl</p>
              </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src={sliderone}
                height="800"
                alt="Third slide"
              />

              <Carousel.Caption>
                <h3 className="SUNSET-VILLA">SUNSET VILLA</h3>
                <p className="Amazing-view">Amazing view - Cave house - SPA - Breakfast incl</p>
              </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src={sliderone}
                height="800"
                alt="Third slide"
              />

              <Carousel.Caption>
                <h3 className="SUNSET-VILLA">SUNSET VILLA</h3>
                <p className="Amazing-view">Amazing view - Cave house - SPA - Breakfast incl</p>
              </Carousel.Caption>
            </Carousel.Item>
          </Carousel>
        </section>

        <section class="search-sec">
          <div class="container">
            <form >

              <div class="row">
                <div class="col-lg-12">
                  <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-12 p-0">
                      <DatePickers />
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 p-0">
                      <div class="input-group mb-3">
                        <input type="text" class="form-control  search-slt" placeholder="Santorini" id="demo" name="email" />
                        <div class="input-group-append">
                          <span class="input-group-text">   <FiMapPin className="Icon-feather-filter" /> </span>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 p-0">
                      <div class="input-group mb-3">
                        <input type="text" class="form-control  search-slt" placeholder="Invitées" id="demo" name="email" />
                        <div class="input-group-append">
                          <span class="input-group-text">   <FiUsers className="Icon-feather-filter" /> </span>
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 p-0">
                      <button type="button" class="btn  wrn-btn">Search</button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </section>
      </header>
    );
  }
}

export default Header;


