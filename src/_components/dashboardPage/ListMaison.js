import React, { Component } from 'react';
import Card from "react-bootstrap/Card";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import { FiAlignLeft, FiFilter } from "react-icons/fi";
import { openService } from '../../_shared/services/open/open.service';

class ListMaison extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        
        this.state = { value: 'Type some lyrics here' };

    }

    handleChange(e) {
        this.setState({ value: e.target.value });
    }






    render() {
        const { listHouses } = this.props  
        
     
        return (
           
            <Container>
                <Row>
                    <Col className="Maison-dhte-disponible">
                        Maison d’hôte
                </Col>

                </Row>


                {listHouses.houseList &&
                    <Row>
                        {listHouses.houseList.map((house, index) =>
                            <Col key={house.id}>
                                <Card style={{ width: '18rem' }} className="card-houses">
                                    <Card.Img variant="top" src={house.cover.web_path} />
                                    <Card.Body>
                                        <Card.Title>{house.name}</Card.Title>
                                        <Card.Text>
                                            {house.description}
                                        </Card.Text>

                                    </Card.Body>
                                </Card>
                            </Col>
                        )}
                    </Row>
                }
                <div class="col-lg-3 col-md-3 col-sm-12 p-0">
                    <button type="button" class="btn  wrn-btn btn-aff">Afficher Tous</button>
                </div>

            </Container>
            

        );
     
    }
}
export default ListMaison;