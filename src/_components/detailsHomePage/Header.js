import React, { Component } from 'react';
import Carousel from 'react-bootstrap/Carousel';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { NavLink } from "react-router-dom";
import Logowerzen from '../../image/images/logo/Logowerzen.png';
import sliderone from '../../image/images/sliderimgs/sliderone.png';

class Header extends Component {
  render() {
    const { detailsHouse } = this.props
    console.log('header---->', detailsHouse)
    return (
      <header> 
        <section class="search-sec">
          <div class="container">
            <form >

              <Navbar collapseOnSelect expand="lg" variant="dark">
                <Navbar.Brand href="#home">
                  <img
                    src={Logowerzen}
                    width="30"
                    height="30"
                    className="d-inline-block align-top"
                    alt="React Bootstrap logo"
                  />
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                  <Nav className="mr-auto">
                  </Nav>
                  <Nav>
                    <Nav.Link href="#deets">More deets</Nav.Link>
                    <Nav.Link href="#deets">More deets</Nav.Link>
                    <Nav.Link eventKey={2} href="#memes">
                      Dank memes
      </Nav.Link>
                  </Nav>
                </Navbar.Collapse>
              </Navbar>

            </form>
          </div>
        </section>

        <section >
          <Carousel className="hello-lightbulb-YC8qqp50BdA-unsplash" >
            <Carousel.Item>

              <img
                className="d-block w-100"
                src={detailsHouse[0].cover.web_path} 
                alt="First slide"
              />
              <Carousel.Caption>
                <h3 className="SUNSET-VILLA" >SUNSET VILLA</h3>
                <p className="Amazing-view---Cave-house---SPA---Breakfast-incl">Amazing view - Cave house - SPA - Breakfast incl</p>
              </Carousel.Caption>
            </Carousel.Item>
          </Carousel>
        </section>





      </header>
    );
  }
}

export default Header;