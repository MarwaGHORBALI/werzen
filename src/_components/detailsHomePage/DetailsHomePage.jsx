import React from 'react';
import { connect } from 'react-redux';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import { Router, Route, Link } from 'react-router-dom';
import * as Yup from 'yup';
import SignInStyleWrapper from './details.style.js';
import { history } from '../../_helpers';
import { alertActions } from '../../_actions';


import Header from './Header';
import Details from './Details';




class DetailsHomePage extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id: props.match.params.id,
        }
        history.listen((location, action) => {
            // clear alert on location change
            this.props.clearAlerts();
        });


    }



    render() {
        const { housesList } = this.props;
        return (

            <Formik
                initialValues={{
                    email: '',
                    password: '',
                }}

                validationSchema={Yup.object().shape({
                    email: Yup.string()
                        .email('Email is invalid')
                        .required('Email is required'),
                    password: Yup.string()
                        .required('Password is required'),

                })}

                onSubmit={fields => {
                    alert('SUCCESS!! :-)\n\n' + JSON.stringify(fields, null, 4))
                }}

                render={({ errors, status, touched }) => (
                    <SignInStyleWrapper className="isoSignInPage">
                        <div className="wrapper">
                        
                            {housesList.houseList &&
                                    <Header detailsHouse=
                                        {housesList.houseList.map((houseDetail, index) => {
                                            if (this.state.id == houseDetail.id) {
                                                return houseDetail;
                                            }
                                        }
                                        )}
                                    />
                                }
                            <div className="workspace">
                                {housesList.houseList &&
                                    <Details detailsHouse=
                                        {housesList.houseList.map((houseDetail, index) => {
                                            if (this.state.id == houseDetail.id) {
                                                return houseDetail;
                                            }
                                        }
                                        )}
                                    />
                                }

                            </div>
                        </div>
                    </SignInStyleWrapper>
                )}
            />
        )
    }
}


function mapState(state) {
    const { alert, housesList } = state;
    return { alert, housesList };
}

const actionCreators = {
    clearAlerts: alertActions.clear
};

const connecteddetailsHome = connect(mapState, actionCreators)(DetailsHomePage);
export { connecteddetailsHome as DetailsHomePage };


