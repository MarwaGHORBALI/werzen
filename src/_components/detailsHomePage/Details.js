import React, { Component } from 'react';
import Card from "react-bootstrap/Card";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';

class Details extends Component {
    constructor(props) {
        super(props);
     
    }

    render() {
        const { detailsHouse } = this.props 
        console.log('details---->',detailsHouse)

        return (
            <footer class="page-footer font-small blue pt-4">
                <div class="container-fluid text-center text-md-left">
                    <div class="row">
                        <div class="col-md-6 mt-md-0 mt-3">
                            <h5 class="text-uppercase">{detailsHouse[0].name}</h5>
                            <p>{detailsHouse[0].description}</p>
                        </div>
                        <div class="col-md-6 mt-md-0 mt-3">
                            <Card style={{ width: '18rem' }}>
                                <Card.Img variant="top" src="holder.js/100px180" />
                                <Card.Body>
                                    <Card.Title>Card Title</Card.Title>
                                    <Card.Text>
                                        Some quick example text to build on the card title and make up the bulk of
                                        the card's content.
                 </Card.Text>
                                    <Button variant="primary">Go somewhere</Button>
                                </Card.Body>
                            </Card>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="col-md-6 mt-md-0 mt-3">
                            <Container>
                                <Row>
                                    <Col className="Maison-dhte-disponible">
                                        Maison d’hôte
                </Col>
                                </Row>

                                <Row>
                                    <Col>
                                        <Card style={{ width: '18rem' }}>
                                            <Card.Img variant="top" src="holder.js/100px180" />
                                            <Card.Body>
                                                <Card.Title>Card Title</Card.Title>
                                                <Card.Text>
                                                    Some quick example text to build on the card title and make up the bulk of
                                                    the card's content.
                 </Card.Text>
                                                <Button variant="primary">Go somewhere</Button>
                                            </Card.Body>
                                        </Card>
                                    </Col>
                                    <Col>

                                        <Card style={{ width: '18rem' }}>
                                            <Card.Img variant="top" src="holder.js/100px180" />
                                            <Card.Body>
                                                <Card.Title>Card Title</Card.Title>
                                                <Card.Text>
                                                    Some quick example text to build on the card title and make up the bulk of
                                                    the card's content.
                 </Card.Text>
                                                <Button variant="primary">Go somewhere</Button>
                                            </Card.Body>
                                        </Card>
                                    </Col>
                                    
                                </Row>
                            </Container>

                        </div>

                        <div class="col-md-6 mt-md-0 mt-3">
                            
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}

export default Details;