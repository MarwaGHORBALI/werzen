import { openConstants } from '../_constants';
import { housesConstants } from '../_constants';

export function open(state = {}, action) { 
    switch (action.type) {
        case openConstants.GETALL_REQUEST:
            return {
                loading: true
            };
        case openConstants.GETALL_SUCCESS:
            console.log('offers', action.listOffers)
            return {
                offers: action.listOffers
            };
        case openConstants.GETALL_FAILURE:
            return {
                error: action.error
            };
        default:
            return state
    }
}


export function housesList(state = {}, action) { 
    switch (action.type) {
        case housesConstants.GETALL_REQUEST:
            return {
                loading: true
            };
        case housesConstants.GETALL_SUCCESS:
            return {
                houseList: action.houses
            };
        case housesConstants.GETALL_FAILURE:
            return {
                error: action.error
            };
        default:
            return state
    }
}