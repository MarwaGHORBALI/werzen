import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import { registration } from './registration.reducer';
import { users } from './users.reducer';
import { alert } from './alert.reducer';
import { open } from './open.reducer';
import { housesList } from './open.reducer';

const rootReducer = combineReducers({
  authentication,
  registration,
  users,
  alert,
  housesList,
  open
});

export default rootReducer;