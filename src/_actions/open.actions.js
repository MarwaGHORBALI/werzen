import { openConstants } from '../_constants';
import { housesConstants } from '../_constants';
import { openService } from '../_shared/services/open/open.service';
import { alertActions } from './';
import { history } from '../_helpers';

export const openActions = {
    getListHouses,
    getListOffers
};

//G.M : action get Houses Offers
function getListHouses() {
    return dispatch => {
        dispatch(request());

        openService.getListHouses() 
            .then(
                houses => dispatch(success(houses)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: housesConstants.GETALL_REQUEST } }
    function success(houses) { return { type: housesConstants.GETALL_SUCCESS, houses } }
    function failure(error) { return { type: housesConstants.GETALL_FAILURE, error } }
}

//G.M : action get List Offers
function getListOffers() {
    return dispatch => {
        dispatch(request());

        openService.getListOffers() 
            .then(
                listOffers => dispatch(success(listOffers)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: openConstants.GETALL_REQUEST } }
    function success(listOffers) { return { type: openConstants.GETALL_SUCCESS, listOffers } }
    function failure(error) { return { type: openConstants.GETALL_FAILURE, error } }
}

/*function register(user) {
    return dispatch => {
        dispatch(request(user));

        userService.register(user)
            .then(
                user => { 
                    dispatch(success());
                    history.push('/login');
                    dispatch(alertActions.success('Registration successful'));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request(user) { return { type: userConstants.REGISTER_REQUEST, user } }
    function success(user) { return { type: userConstants.REGISTER_SUCCESS, user } }
    function failure(error) { return { type: userConstants.REGISTER_FAILURE, error } }
}

function getAll() {
    return dispatch => {
        dispatch(request());

        userService.getAll()
            .then(
                users => dispatch(success(users)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: userConstants.GETALL_REQUEST } }
    function success(users) { return { type: userConstants.GETALL_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_FAILURE, error } }
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
    return dispatch => {
        dispatch(request(id));

        userService.delete(id)
            .then(
                user => dispatch(success(id)),
                error => dispatch(failure(id, error.toString()))
            );
    };

    function request(id) { return { type: userConstants.DELETE_REQUEST, id } }
    function success(id) { return { type: userConstants.DELETE_SUCCESS, id } }
    function failure(id, error) { return { type: userConstants.DELETE_FAILURE, id, error } }
}*/